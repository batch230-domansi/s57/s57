let collection = [];

// Write the queue functions below.
function print(){
	return collection;
}

function enqueue(element) {
  collection[collection.length] = element;
  return collection;
}

function dequeue() {
  const dequeuedElement = collection[0];
  for (let i = 0; i < collection.length - 1; i++) {
    collection[i] = collection[i+1];
  }
  collection.length--;
  return collection;
}

function front() {
  if (collection.length > 0) {
    return collection[0];
  } else {
    return undefined;
  }
}

function size() {
  return collection.length;
}

function isEmpty() {
  return collection.length === 0;
}

module.exports = {
  print,
  enqueue,
  dequeue,
  front,
  size,
  isEmpty
};